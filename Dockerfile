# Step 1: Build
FROM openjdk:8 AS build

RUN mkdir /appbuild
COPY src /appbuild/src
COPY gradle /appbuild/gradle
COPY [ "build.gradle.kts", "gradlew", "settings.gradle.kts", "gradle.properties", "/appbuild/"]
WORKDIR /appbuild
RUN ./gradlew clean shadowjar

# tag::production[]
# Step 2: Runtime
FROM openjdk:8-jre-alpine

RUN apk add --no-cache fortune
RUN mkdir /app
RUN chown -R 1001:0 /app
RUN chmod -R 755 /app
COPY --from=build /appbuild/build/libs/cloud.appuio.fortune-kotlin*all.jar /app/cloud.appuio.fortune-kotlin.jar
WORKDIR /app

EXPOSE 8080

# <1>
USER 1001:0

CMD ["sh", "-c", "java -server -XX:+UnlockExperimentalVMOptions -XX:+UseCGroupMemoryLimitForHeap -XX:InitialRAMFraction=2 -XX:MinRAMFraction=2 -XX:MaxRAMFraction=2 -XX:+UseG1GC -XX:MaxGCPauseMillis=100 -XX:+UseStringDeduplication -jar cloud.appuio.fortune-kotlin.jar"]
# end::production[]
