package cloud.appuio.plugins

import com.github.mustachejava.DefaultMustacheFactory
import io.ktor.mustache.Mustache
import io.ktor.mustache.MustacheContent
import io.ktor.routing.*
import io.ktor.http.*
import io.ktor.application.*
import io.ktor.response.*
import io.ktor.request.*
import io.ktor.gson.*
import io.ktor.features.*
import java.util.concurrent.TimeUnit
import java.io.IOException

data class Fortune(val number: Int, val message: String, val version: String, val hostname: String)

// Adapted from https://stackoverflow.com/a/41495542
fun String.runCommand(): String {
    try {
        val parts = this.split("\\s".toRegex())
        val proc = ProcessBuilder(*parts.toTypedArray())
                .redirectOutput(ProcessBuilder.Redirect.PIPE)
                .redirectError(ProcessBuilder.Redirect.PIPE)
                .start()

        proc.waitFor(60, TimeUnit.MINUTES)
        return proc.inputStream.bufferedReader().readText()
    } catch(e: IOException) {
        e.printStackTrace()
        return ""
    }
}

// tag::router[]
fun Application.configureRouting() {
    install(Mustache) {
        mustacheFactory = DefaultMustacheFactory("templates")
    }
    install(ContentNegotiation) {
        gson()
    }

    val version = "1.2-kotlin"
    val hostname = "hostname".runCommand()

    routing {
        get("/") {
            val fortune = Fortune((1..1000).random(),
                                  "fortune".runCommand(),
                                  version,
                                  hostname)
            val accept = call.request.accept() ?: ""
            val text = """Fortune ${fortune.version} cookie of the day ${fortune.number}:

${fortune.message}
Pod: ${fortune.hostname}"""

            when (accept) {
                "application/json" -> call.respond(fortune)
                "text/plain" -> call.respondText(text)
                else -> call.respond(MustacheContent("fortune.hbs", mapOf("fortune" to fortune)))
            }
        }
    }
}
// end::router[]
